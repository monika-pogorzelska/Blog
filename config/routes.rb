Rails.application.routes.draw do
  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'

  get 'users/new'

  get '/index', to: 'welcome#index'

  get '/home', to: 'welcome#home'

  get '/about', to: 'welcome#about'

  get '/help', to: 'welcome#help'

  get '/contact', to: 'welcome#contact'

  get '/signup', to: 'users#new'
  
  post '/signup',  to: 'users#create'

  get    '/login',   to: 'sessions#new'

  post   '/login',   to: 'sessions#create'
  
  delete '/logout',  to: 'sessions#destroy'

  resources :articles do
    resources :comments
  end

  resources :users do
    member do
      get :following, :followers
    end
  end

  resources :account_activations, only: [:edit]

  resources :password_resets,     only: [:new, :create, :edit, :update]

  resources :microposts,          only: [:create, :destroy]

  resources :relationships,       only: [:create, :destroy]

  root 'welcome#home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
