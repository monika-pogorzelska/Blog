require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  def setup
    @article = Article.new(title: "Example Article", text: "bla bla bla")
  end

  test "should be valid" do
    assert @article.valid?
  end

  test "text should be present" do
    @article.text = "     "
    assert_not @article.valid?
  end

  test "title should be present" do
    @article.title = ""
    assert_not @article.valid?
  end
end

