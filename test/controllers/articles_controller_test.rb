require 'test_helper'

class ArticlesControllerTest < ActionDispatch::IntegrationTest

  test "should get new" do
    get new_article_path
    assert_response :success
    assert_select "title", "Blog"
  end

  test "should get articles" do
    get articles_path
    assert_response :success
    assert_select "title", "all articles"
  end
 
  test "should edit article" do
    article = articles(:monika)
    get edit_article_path(article)
    assert_response :success
  end

end
