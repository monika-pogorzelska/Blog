require 'test_helper'

class ArticlesAddingTest < ActionDispatch::IntegrationTest
  test "invalid article" do
    get articles_path
    assert_no_difference 'Article.count' do
      post articles_path, params: { article: {title: "coco", 
                                    text: "chanel"} }
    end
    assert_template 'articles/new'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end

  test "valid article" do
    assert_difference 'Article.count', 1 do
      post articles_path, params: { article: {title: "coco roko", 
                                    text: "chanel number 5 is the best!"} }
    end
    follow_redirect!
    assert_template 'articles/show'
    assert_not flash.empty?
  end
end
 