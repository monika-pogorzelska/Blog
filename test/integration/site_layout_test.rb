require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
 test "home page links" do
    get root_path
    assert_template 'welcome/home'
    assert_select "a[href=?]", root_path
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
    assert_select "a[href=?]", articles_path
    assert_select "a[href=?]", signup_path
  end

  test "articles page links" do
    get articles_path
    article = articles(:monika)
    assert_template 'articles/index'
    assert_select "a[href=?]", new_article_path
    assert_select "a[href=?]", article_path(article)
    assert_select "a[href=?]", edit_article_path(article)
  end
end
